// import 'regenerator-runtime/runtime'
import { Metadata } from 'next'

import { Toaster } from 'react-hot-toast'

import '@/app/globals.css'
import { fontMono, fontSans } from '@/lib/fonts'
import { cn } from '@/lib/utils'
import { TailwindIndicator } from '@/components/tailwind-indicator'
import { Providers } from '@/components/providers'
import { Header } from '@/components/header'

export const metadata: Metadata = {
  title: {
    default: 'NimblAI Healthcare Chatbot',
    template: `%s - NimblAI Chatbot`
  },
  description: 'An AI-powered chatbot with context support for your website.',
  themeColor: [
    { media: '(prefers-color-scheme: light)', color: 'white' },
    { media: '(prefers-color-scheme: dark)', color: 'black' }
  ],
  icons: {
    icon: '/nimbl-favicon-large.png',
    shortcut: '/nimbl-favicon-large.png',
    apple: '/apple-touch-icon.png'
  }
}

interface RootLayoutProps {
  children: React.ReactNode
}

export default function RootLayout({ children }: RootLayoutProps) {
  return (
    <html lang="en" suppressHydrationWarning>
      <head />
      <body
        className={cn(
          'font-sans antialiased',
          fontSans.variable,
          fontMono.variable
        )}
      >
        <Toaster />
        <Providers attribute="class" defaultTheme="dark" enableSystem>
        
          <div className="flex flex-col min-h-screen">
            {/* @ts-ignore */}
            <Header />
            <main 
            // style={{
            //   backgroundImage: `url("/main-bg.jpg`,
            //   backgroundSize: `cover`,
            //   backgroundAttachment: `fixed`,
            //   backgroundPosition: `center`,
            //   backgroundRepeat: `repeat-y`,
            // }} 
            // bg-muted/50
            className="flex flex-col flex-1 bg-muted/50">
              {/* <div className="absolute z-0 h-[40px] md:h-[110px] w-full inset-x-0 top-0 [background:linear-gradient(180deg,_#121111,_rgba(18,_17,_17,_0.99)_6.67%,_rgba(19,_17,_17,_0.96)_13.33%,_rgba(19,_17,_17,_0.92)_20%,_rgba(21,_17,_17,_0.85)_26.67%,_rgba(22,_17,_17,_0.77)_33.33%,_rgba(24,_17,_17,_0.67)_40%,_rgba(26,_17,_17,_0.56)_46.67%,_rgba(28,_17,_17,_0.44)_53.33%,_rgba(29,_17,_17,_0.33)_60%,_rgba(31,_17,_17,_0.23)_66.67%,_rgba(33,_17,_17,_0.15)_73.33%,_rgba(34,_17,_17,_0.08)_80%,_rgba(34,_17,_17,_0.04)_86.67%,_rgba(35,_17,_17,_0.01)_93.33%,_rgba(35,_17,_17,_0))]" /> */}

              {children}
              
            </main>
          </div>
          <TailwindIndicator />
        </Providers>
      </body>
    </html>
  )
}
