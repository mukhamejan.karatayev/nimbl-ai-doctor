import * as React from 'react'
import Link from 'next/link'

import { cn } from '@/lib/utils'
// import { auth } from '@/auth'
// import { clearChats } from '@/app/actions'
import { Button, buttonVariants } from '@/components/ui/button'
import { Sidebar } from '@/components/sidebar'
import { SidebarList } from '@/components/sidebar-list'
import {
  IconGitHub,
  IconNextChat,
  IconSeparator,
  IconVercel
} from '@/components/ui/icons'
import { SidebarFooter } from '@/components/sidebar-footer'
import { ThemeToggle } from '@/components/theme-toggle'
import { ClearHistory } from '@/components/clear-history'
import { UserMenu } from '@/components/user-menu'
import { LoginButton } from '@/components/login-button'
import Image from 'next/image'
// import { useTheme } from 'next-themes'

export async function Header() {
  // const session = await auth()
  // const { setTheme, theme } = useTheme()
  return (
    <header className="sticky top-0 z-50 flex items-center justify-between w-full h-16 px-4 border-b shrink-0 bg-gradient-to-b from-background/10 via-background/50 to-background/80 backdrop-blur-xl">
      <div className="flex items-center justify-between w-full">
        <div className="flex items-center pr-4 md:pr-0">
          {/* target="_blank" rel="nofollow" */}
          {/* <p>{theme}</p> */}
          <Link href="/">

            {/* <IconNextChat className="w-6 h-6 mr-2 dark:hidden" inverted />
            <IconNextChat className="hidden w-6 h-6 mr-2 dark:block" /> */}
            <div className='flex md:hidden'>
            <Image className='' src="/nimbl-favicon-large.png" alt="logo" width={40} height={40} />
            
            </div>
            <div className='hidden md:flex items-center'>
            <Image className='' src="/nimbl-favicon-large.png" alt="logo" width={50} height={50} />
            {/* <p className='px-2 text-2xl font-sans font-extrabold'>doctor.ai</p> */}
            </div>
            
          </Link>
          {/* <div className='flex md:hidden pl-2 md:pl-4'>
            <Image className='' src="/digital bridge 2023 text.svg" alt="logo-text" width={100} height={50} />
          </div>
          <div className='hidden md:flex pl-2 md:pl-4'>
            <Image className='' src="/digital bridge 2023 text.svg" alt="logo-text" width={150} height={50} />
          </div> */}
        </div>
        <div className='flex items-center'>
        {/* <ThemeToggle /> */}

        {/* <IconSeparator className="w-6 h-6 text-muted-foreground/50" /> */}
        {/* {session?.user ? (
          <Sidebar>
            <React.Suspense fallback={<div className="flex-1 overflow-auto" />}>
              <SidebarList userId={session?.user?.id} />
            </React.Suspense>
            <SidebarFooter>
              <ThemeToggle />
              <ClearHistory clearChats={clearChats} />
            </SidebarFooter>
          </Sidebar>
          
        ) : (
          <Link href="/" target="_blank" rel="nofollow">
            <IconNextChat className="w-6 h-6 mr-2 dark:hidden" inverted />
            <IconNextChat className="hidden w-6 h-6 mr-2 dark:block" />
          </Link>
        )} */}
        
        <div className="flex items-center">
          
          {/* <IconSeparator className="w-6 h-6 text-muted-foreground/50" /> */}
          {/* {session?.user ? (
            <UserMenu user={session.user} />
          ) : (
            <Button variant="link" asChild className="-ml-2">
              <Link href="/sign-in?callbackUrl=/">Login</Link>
            </Button>
          )} */}
        </div>

        </div>
        
      </div>
      {/* <div className="flex items-center justify-end space-x-2">
        <a
          target="_blank"
          href="https://github.com/vercel/nextjs-ai-chatbot/"
          rel="noopener noreferrer"
          className={cn(buttonVariants({ variant: 'outline' }))}
        >
          <IconGitHub />
          <span className="hidden ml-2 md:flex">GitHub</span>
        </a>
        <a
          href="https://github.com/vercel/nextjs-ai-chatbot/"
          target="_blank"
          className={cn(buttonVariants())}
        >
          <IconVercel className="mr-2" />
          <span className="hidden sm:block">Deploy to Vercel</span>
          <span className="sm:hidden">Deploy</span>
        </a>
      </div> */}
    </header>
  )
}
